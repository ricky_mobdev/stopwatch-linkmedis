import { Component } from '@angular/core';
import { Platform, IonicApp, ToastController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { BackgroundMode } from '@ionic-native/background-mode';
import { AppMinimize } from '@ionic-native/app-minimize';
import { StopwatchProvider } from '../providers/stopwatch/stopwatch';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public ionic: IonicApp, public toastCtrl: ToastController, public app: App, public backgroundMode: BackgroundMode, public appMinimize: AppMinimize, public stopWatch: StopwatchProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (platform.is('ios')) {
        statusBar.overlaysWebView(false);
        statusBar.styleDefault();
      } else {
        statusBar.styleLightContent();
      }
      splashScreen.hide();
      this.backgroundMode.on('activate').subscribe(() => {
        // Call your method here
      });
      this.backgroundMode.enable();
      this.backButtonInitalilze();
    });

  }

  backButtonInitalilze() {
    let lastTimeBackPress = 0;
    const timePeriodToExit = 2000;
    this.platform.registerBackButtonAction(() => {
      const activePortal =
        this.ionic._loadingPortal.getActive() ||
        this.ionic._modalPortal.getActive() ||
        this.ionic._overlayPortal.getActive();
      const nav = this.app.getRootNav();
      if (activePortal && activePortal.index === 0) {
        activePortal.dismiss();
        return;
        // tslint:disable-next-line: no-else-after-return
      } else if (nav.canGoBack()) {
        nav.pop();
      } else {
        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
          if (this.stopWatch.running) {
            this.appMinimize.minimize();
          } else {
            this.backgroundMode.disable();
            this.platform.exitApp();
          }

          // this.platform.();
        } else {
          const toast = this.toastCtrl.create({
            message: 'Press once again to exit app',
            duration: 3000,
            position: 'bottom',

          });
          toast.present();

          lastTimeBackPress = new Date().getTime();
        }
      }
    });
  }
}

