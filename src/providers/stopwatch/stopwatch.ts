import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StopwatchProvider {
  public Interval;

  public timeBegan = null
  public timeStopped: any = null
  public stoppedDuration: any = 0
  public started = null
  public running = false
  public blankTime = "00:00.00:00"
  public time = "00:00.00:00"
  splitsTime: any = []
  newStoppedDuration: any;
  statusStorage: boolean = false;
  constructor(private storage: Storage) {
    this.storage.get('stopwatchTime').then((resp) => {
      if (resp) {
        clearInterval(resp.started);
        this.time = resp.time
        this.stoppedDuration = resp.stoppedDuration
        this.timeStopped = resp.timeStopped
        this.timeBegan = resp.timeBegan
        this.statusStorage = true
        this.newStoppedDuration = resp.newStoppedDuration
        this.splitsTime = resp.splitsTime
      }
    })
  }

  start() {
    if (this.running) {
      return
    };
    if (this.timeBegan === null) {
      this.reset();
      this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      if(!this.statusStorage) {
        this.newStoppedDuration = (+new Date() - this.timeStopped)
      } else {
        this.timeStopped = new Date(this.timeStopped)
        this.newStoppedDuration = (+new Date() - this.timeStopped)
      }
      this.stoppedDuration = this.stoppedDuration + this.newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 10);
    this.running = true;

  }
  pause() {
    
    this.running = false;
    this.timeStopped = new Date();
    clearInterval(this.started);
    let data = {};
    data = {
      timeBegan: this.timeBegan,
      time: this.time,
      newStoppedDuration: this.newStoppedDuration,
      stoppedDuration: this.stoppedDuration,
      timeStopped: this.timeStopped,
      started: this.started,
      splitsTime: this.splitsTime,
    }
    this.storage.set('stopwatchTime', data)
  }

  split() {
    this.splitsTime.push(this.time);
  }

  stop() {
    this.running = false;
    clearInterval(this.started);
    this.stoppedDuration = 0;
    this.timeBegan = null;
    this.timeStopped = null;
    this.time = this.blankTime;
    this.storage.clear();
    this.statusStorage = false;
  }
  reset() {
    this.statusStorage = false;
    this.storage.clear();
    this.running = false;
    clearInterval(this.started);
    this.stoppedDuration = 0;
    this.timeBegan = null;
    this.timeStopped = null;
    this.time = this.blankTime;
    this.splitsTime = [];
  }
  zeroPrefix(num, digit) {
    let zero = '';
    for (let i = 0; i < digit; i++) {
      zero += '0';
    }
    return (zero + num).slice(-digit);
  }
  clockRunning() {
    let currentTime: any = new Date()
    let timeElapsed: any = new Date()
    if(!this.statusStorage) {
       timeElapsed = new Date(currentTime - this.timeBegan - this.stoppedDuration)
    } else {
      let a : any = new Date(currentTime);
      let b : any = new Date(this.timeBegan);
      let c : any = new Date(this.stoppedDuration);
      timeElapsed = new Date(a - b - c);
      console.log(timeElapsed, a , b, c);
    }
    let hour = timeElapsed.getUTCHours()
    let min = timeElapsed.getUTCMinutes()
    let sec = timeElapsed.getUTCSeconds()
    let ms = timeElapsed.getUTCMilliseconds();
    this.time =
      this.zeroPrefix(hour, 2) + ":" +
      this.zeroPrefix(min, 2) + ":" +
      this.zeroPrefix(sec, 2) + "." +
      this.zeroPrefix(ms, 2);
  };

}
