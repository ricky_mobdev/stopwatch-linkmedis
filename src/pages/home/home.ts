import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StopwatchProvider } from '../../providers/stopwatch/stopwatch';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public stopWatch: StopwatchProvider) {

  }

}
